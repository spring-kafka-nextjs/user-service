package com.service.user.config;

import com.service.user.websocket.ResendTokenHandler;
import com.service.user.websocket.UserWebSocketHandler;
import com.service.user.websocket.ValidateEmailHandler;
import com.service.user.websocket.ValidateUsernameHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    private final UserWebSocketHandler userWebSocketHandler;
    private final ResendTokenHandler resendTokenHandler;
    private final ValidateUsernameHandler validateUsernameHandler;
    private final ValidateEmailHandler validateEmailHandler;

    private final String[] allowedOrigins = {"http://localhost:3000"};

    @Autowired
    public WebSocketConfig(UserWebSocketHandler userWebSocketHandler, ResendTokenHandler resendTokenHandler, ValidateUsernameHandler validateUsernameHandler, ValidateEmailHandler validateEmailHandler) {
        this.userWebSocketHandler = userWebSocketHandler;
        this.resendTokenHandler = resendTokenHandler;
        this.validateUsernameHandler = validateUsernameHandler;
        this.validateEmailHandler = validateEmailHandler;
    }

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(userWebSocketHandler, "/ws/register")
                .setAllowedOrigins(allowedOrigins);
        registry.addHandler(resendTokenHandler, "/ws/resend-token")
                .setAllowedOrigins(allowedOrigins);
        registry.addHandler(validateUsernameHandler, "/ws/validate-username")
                .setAllowedOrigins(allowedOrigins);
        registry.addHandler(validateEmailHandler, "/ws/validate-email")
                .setAllowedOrigins(allowedOrigins);
    }
}