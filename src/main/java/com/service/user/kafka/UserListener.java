package com.service.user.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.service.user.dto.UserDTO;
import com.service.user.dto.UserTokenDTO;
import com.service.user.service.UserService;
import com.service.user.utils.DynamicModelMapper;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class UserListener {

    @Autowired
    private DynamicModelMapper modelMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private Validator validator;

    @Autowired
    private KafkaProducer kafkaProducer;

    @KafkaListener(topics = "register-user", groupId = "user-group")
    public void registerProduct(String message) {
        modelMapper.mapToModel(message, UserDTO.class)
                .subscribe(msg -> {
                    Set<ConstraintViolation<UserDTO>> violations = validator.validate(msg);
                    if (violations.isEmpty()) {
                        userService.registerUser(msg).subscribe();
                    } else {
                        for (ConstraintViolation<UserDTO> violation : violations) {
                            try {
                                kafkaProducer.sendMessage("register-user-failed", violation.getMessage());
                            } catch (JsonProcessingException e) {
                                throw new RuntimeException(e);
                            }
                        }
                    }
                }, error -> {
                    try {
                        kafkaProducer.sendMessage("register-user-failed", error.getMessage());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                });
    }
}