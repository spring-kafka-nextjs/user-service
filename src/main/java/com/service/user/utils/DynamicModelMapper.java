package com.service.user.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

@Component
public class DynamicModelMapper {

    @Autowired
    private ObjectMapper objectMapper;

    public <T> Mono<T> mapToModel(String jsonContent, Class<T> modelClass) {
        return Mono.fromCallable(() -> objectMapper.readValue(jsonContent, modelClass));
    }
}