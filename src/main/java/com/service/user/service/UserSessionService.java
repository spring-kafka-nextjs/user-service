package com.service.user.service;

import com.service.user.dto.UserSessionDTO;
import com.service.user.model.UserSession;
import reactor.core.publisher.Mono;

public interface UserSessionService {
    Mono<UserSession> createUserSession(UserSessionDTO userSessionDTO);

    Mono<Object> getUserSession(String sessionId);
}
