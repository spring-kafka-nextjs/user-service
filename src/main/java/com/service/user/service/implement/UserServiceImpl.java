package com.service.user.service.implement;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.service.user.dto.ConfirmationMessageKafka;
import com.service.user.dto.UserDTO;
import com.service.user.dto.UserTokenDTO;
import com.service.user.kafka.KafkaProducer;
import com.service.user.model.User;
import com.service.user.model.UserStatus;
import com.service.user.model.UserToken;
import com.service.user.repository.UserRepository;
import com.service.user.repository.UserTokenRepository;
import com.service.user.service.UserService;
import com.service.user.service.UserSessionService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private KafkaProducer kafkaProducer;

    @Autowired
    private UserSessionService userSessionService;

    @Autowired
    private UserTokenRepository userTokenRepository;

    @Autowired
    private ReactiveMongoTemplate reactiveMongoTemplate;

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public Mono<User> registerUser(UserDTO msg) {
        User user = msg.toUser();
        String token = generateUserToken(msg.getUsername());
        return userRepository.save(user)
                .flatMap(savedUser -> sendKafkaMessages(savedUser, new ConfirmationMessageKafka().setUsername(user.getUsername()).setEmail(user.getEmail()).setToken(token))
                                .then(userTokenRepository.save(UserToken.builder()
                                        .token(token)
                                        .username(savedUser.getUsername())
                                        .expired(System.currentTimeMillis() + 1000 * 60 * 30)
                                        .build()))
                                .then(userSessionService.createUserSession(msg.toUserSession()))
                        .thenReturn(savedUser)
                )
                .onErrorResume(error -> {
                    try {
                        kafkaProducer.sendMessage("register-failed", user);
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return Mono.empty();
                });
    }
    @Override
    public Mono<String> activatedUser(String token) {
        return userTokenRepository.findByToken(token)
                .switchIfEmpty(Mono.error(new IllegalArgumentException("User token not found for provided token.")))
                .doOnNext(userToken -> logger.info("Found user token: {}", userToken.getToken()))
                .flatMap(userToken -> {

                    if (System.currentTimeMillis() > userToken.getExpired()) {
                        return Mono.error(new IllegalArgumentException("Token has expired."));
                    }

                    String username = userToken.getUsername();
                    return userRepository.findByUsernameAndStatus(username, UserStatus.INACTIVE)
                            .switchIfEmpty(Mono.error(new IllegalArgumentException("User not found for provided username.")))
                            .flatMap(existingUser -> {
                                logger.info("Attempting to activate user: {}", existingUser.getUsername());
                                Query userQuery = new Query();
                                userQuery.addCriteria(Criteria.where("username").is(username).and("status").is(UserStatus.INACTIVE));
                                Update update = new Update();
                                update.set("status", UserStatus.ACTIVE);
                                return reactiveMongoTemplate.updateFirst(userQuery, update, User.class)
                                        .then(Mono.defer(() -> {
                                            Query deleteQuery = new Query();
                                            deleteQuery.addCriteria(Criteria.where("token").is(userToken.getToken()));
                                            return reactiveMongoTemplate.remove(deleteQuery, UserToken.class);
                                        }))
                                        .doOnSuccess(v -> {
                                            try {
                                                kafkaProducer.sendMessage("activated-user-complete", "Success Activated User");
                                            } catch (JsonProcessingException e) {
                                                throw new RuntimeException(e);
                                            }
                                            logger.info("Successfully activated user with username: {}", username);
                                            logger.info("Successfully deleted user token: {}", userToken.getToken());
                                        })
                                        .thenReturn("User Successfully Activated");
                            });
                })
                .doOnError(error -> {
                    logger.error("Error activating user: {}", error.getMessage());
                });
    }


    @Override
    public Mono<String> resendEmail(String receivedMessage) {
        return userRepository.findByUsername(receivedMessage)
                .flatMap(user -> {
                    if (user.getStatus() == UserStatus.ACTIVE) {
                        return Mono.just("User Already Activated");
                    } else {
                        String token = generateUserToken(user.getUsername());
                        Query query = new Query(Criteria.where("username").is(user.getUsername()));
                        return reactiveMongoTemplate.remove(query, UserToken.class)
                                .then(sendKafkaMessages(user, new ConfirmationMessageKafka().setUsername(user.getUsername()).setEmail(user.getEmail()).setToken(token)))
                                .then(userTokenRepository.save(UserToken.builder()
                                        .token(token)
                                        .username(user.getUsername())
                                        .expired(System.currentTimeMillis() + 1000 * 60 * 30)
                                        .build()))
                                .thenReturn("Email successfully sent.");
                    }
                }).onErrorResume(error -> {
                    try {
                        kafkaProducer.sendMessage("resend-email-failed", error.getMessage());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return Mono.just("Error sending email.");
                });
    }

    @Override
    public Mono<String> validateUsername(String receivedMessage) {
        return userRepository.findByUsername(receivedMessage)
                .flatMap(user -> Mono.just("Username is already taken."))
                .switchIfEmpty(
                        Mono.just("Username is available.")
                )
                .onErrorResume(error -> {
                    try {
                        kafkaProducer.sendMessage("validate-username-failed", error.getMessage());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return Mono.just("Error validating username.");
                });
    }

    @Override
    public Mono<String> validateEmail(String receivedMessage) {
        return userRepository.findByEmail(receivedMessage)
                .flatMap(user -> Mono.just("Email is already taken."))
                .switchIfEmpty(
                        Mono.just("Email is available.")
                )
                .onErrorResume(error -> {
                    try {
                        kafkaProducer.sendMessage("validate-email-failed", error.getMessage());
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return Mono.just("Error validating email.");
                });
    }


    private Mono<Void> sendKafkaMessages(User savedUser, ConfirmationMessageKafka confirmationMessageKafka) {
        return Mono.fromRunnable(() -> {
            try {
                kafkaProducer.sendMessage("register-success", savedUser);
                kafkaProducer.sendMessage("register-confirmation", confirmationMessageKafka);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public String generateUserToken(String username) {
        SecureRandom secureRandom = new SecureRandom();
        byte[] randomBytes = new byte[24];
        secureRandom.nextBytes(randomBytes);
        String randomValue = Base64.getUrlEncoder().withoutPadding().encodeToString(randomBytes);

        String combinedValue = username + randomValue;

        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            byte[] hashedBytes = md.digest(combinedValue.getBytes());
            return Base64.getUrlEncoder().withoutPadding().encodeToString(hashedBytes);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("SHA-256 algorithm not found", e);
        }
    }

}
