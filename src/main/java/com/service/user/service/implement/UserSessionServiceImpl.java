package com.service.user.service.implement;

import com.service.user.dto.UserSessionDTO;
import com.service.user.model.UserSession;
import com.service.user.repository.UserSessionRepository;
import com.service.user.service.UserSessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.ReactiveRedisTemplate;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service
public class UserSessionServiceImpl implements UserSessionService{

    @Autowired
    private UserSessionRepository userSessionRepository;

    @Autowired
    private ReactiveRedisTemplate<String, Object> reactiveRedisTemplate;

    @Override
    public Mono<UserSession> createUserSession(UserSessionDTO userSessionDTO) {
        return userSessionRepository.save(userSessionDTO.toUserSession())
                .flatMap(savedSession -> reactiveRedisTemplate.opsForValue()
                        .set(savedSession.getSessionId(), savedSession)
                        .flatMap(success -> {
                            if (success) {
                                return Mono.just(savedSession);
                            } else {
                                System.err.println("Failed to save session to Redis.");
                                return Mono.empty();
                            }
                        }))
                .onErrorResume(e -> {
                    System.err.println("Error creating user session: " + e.getMessage());
                    return Mono.empty();
                });
    }

    @Override
    public Mono<Object> getUserSession(String sessionId) {
        return reactiveRedisTemplate.opsForValue().get(sessionId)
                .switchIfEmpty(
                        userSessionRepository.findById(sessionId)
                                .publishOn(Schedulers.boundedElastic())
                                .doOnNext(session -> {
                                    reactiveRedisTemplate.opsForValue().set(session.getSessionId(), session).subscribe();
                                })
                );
    }

}