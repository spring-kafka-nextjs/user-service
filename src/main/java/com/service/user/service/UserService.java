package com.service.user.service;

import com.service.user.dto.UserDTO;
import com.service.user.dto.UserTokenDTO;
import com.service.user.model.User;
import reactor.core.publisher.Mono;

public interface UserService {
    Mono<User> registerUser(UserDTO msg);

    Mono<String> activatedUser(String token);

    Mono<String> resendEmail(String receivedMessage);

    Mono<String> validateUsername(String receivedMessage);

    Mono<String> validateEmail(String receivedMessage);
}
