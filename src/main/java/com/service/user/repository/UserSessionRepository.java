package com.service.user.repository;

import com.service.user.model.UserSession;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSessionRepository extends ReactiveMongoRepository<UserSession, String> {
}
