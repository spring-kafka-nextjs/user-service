package com.service.user.repository;

import com.service.user.model.UserDetail;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDetailRepository extends ReactiveMongoRepository<UserDetail, String> {
}
