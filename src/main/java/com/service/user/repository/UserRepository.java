package com.service.user.repository;

import com.service.user.model.User;
import com.service.user.model.UserStatus;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {
    Mono<User> findByUsername(String username);

    Mono<User> findByUsernameAndStatus(String username, UserStatus status);

    Mono<User> findByEmail(String email);
}
