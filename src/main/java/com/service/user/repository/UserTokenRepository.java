package com.service.user.repository;

import com.service.user.model.UserToken;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserTokenRepository extends ReactiveMongoRepository<UserToken, String> {
    Mono<UserToken> findByToken(String token);
}
