package com.service.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.service.user.model.UserDetail;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDetailDTO {

    @JsonProperty("first_name")
    private String firstName;

    @JsonProperty("last_name")
    private String lastName;

    @JsonProperty("phone")
    private String phone;

    @JsonProperty("address")
    private String address;

    public UserDetail toUserDetail() {
        return new UserDetail()
                .setFirstName(firstName)
                .setLastName(lastName)
                .setPhone(phone)
                .setAddress(address);
    }
}
