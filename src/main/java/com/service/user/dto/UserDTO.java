package com.service.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.service.user.model.User;
import com.service.user.model.UserRole;
import com.service.user.model.UserStatus;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserDTO {

    @JsonProperty("username")
    @NotBlank(message = "Username cannot be blank")
    @Size(min = 3, max = 30, message = "Username should be between 3 to 30 characters")
    private String username;

    @JsonProperty("email")
    @Email(message = "Invalid email format")
    @NotBlank(message = "Email cannot be blank")
    private String email;

    @JsonProperty("password")
    @NotBlank(message = "Password cannot be blank")
    @Size(min = 6, message = "Password should be at least 6 characters")
    private String password;

    @JsonProperty("role")
    @NotNull(message = "Role cannot be null")
    private UserRole role;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("user_agent")
    private String userAgent;

    public User toUser() {
        return new User()
                .setUsername(username)
                .setEmail(email)
                .setPassword(password)
                .setRole(role)
                .setStatus(UserStatus.INACTIVE);
    }

    public UserSessionDTO toUserSession() {
        return new UserSessionDTO()
                .setUsername(username)
                .setIpAddress(ipAddress)
                .setUserAgent(userAgent);
    }
}
