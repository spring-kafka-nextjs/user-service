package com.service.user.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.service.user.model.UserSession;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class UserSessionDTO {

    @JsonProperty("username")
    private String username;

    @JsonProperty("expired_at")
    private Long expiredAt;

    @JsonProperty("ip_address")
    private String ipAddress;

    @JsonProperty("user_agent")
    private String userAgent;

    public UserSession toUserSession() {
        return new UserSession()
                .setUsername(username)
                .setCreatedAt(System.currentTimeMillis())
                .setExpiredAt(System.currentTimeMillis() + 1000 * 60 * 30)
                .setIpAddress(ipAddress)
                .setUserAgent(userAgent);
    }
}
