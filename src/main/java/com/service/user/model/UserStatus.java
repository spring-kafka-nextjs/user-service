package com.service.user.model;

public enum UserStatus {
    ACTIVE,
    INACTIVE,
    DELETED
}
