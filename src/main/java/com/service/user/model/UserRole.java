package com.service.user.model;

public enum UserRole {
    ADMIN,
    USER,
    GUEST
}