package com.service.user.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "USER_TOKEN")
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@Builder
public class UserToken {
    @Id
    @Field("token_id")
    private String id;

    @Field("user")
    private String username;  // changed from User to String

    @Field("token")
    private String token;

    @Field("expired")
    private Long expired;
}
