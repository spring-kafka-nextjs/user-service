package com.service.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "USER_SESSION")
@Accessors(chain = true)
public class UserSession {

    @Id
    @Field("session_id")
    private String sessionId;

    @Field("username")
    private String username;

    @Field("created_at")
    private Long createdAt;

    @Field("expired_at")
    private Long expiredAt;

    @Field("ip_address")
    private String ipAddress;

    @Field("user_agent")
    private String userAgent;
}
