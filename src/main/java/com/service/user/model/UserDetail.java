package com.service.user.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document(collection = "USER_DETAIL")
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class UserDetail {

    @Id
    @Field("user_detail_id")
    private String id;

    @DBRef
    @Field("user")
    private User user;

    @Field("first_name")
    private String firstName;

    @Field("last_name")
    private String lastName;

    @Field("phone")
    private String phone;

    @Field("address")
    private String address;
}

