package com.service.user.websocket;

import com.service.user.service.UserService;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;

@Component
public class ValidateUsernameHandler extends TextWebSocketHandler {

    private final UserService userService;

    public ValidateUsernameHandler(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws IOException {
        String receivedMessage = (String) message.getPayload();

        userService.validateUsername(receivedMessage)
                .doOnSuccess(responseMessage -> {
                    try {
                        session.sendMessage(new TextMessage(responseMessage));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                })
                .doOnError(e -> {
                    try {
                        session.sendMessage(new TextMessage("Error: " + e.getMessage()));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                })
                .subscribe();
    }
    @Override
    public void afterConnectionEstablished(WebSocketSession session) {

    }
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {

    }
}
