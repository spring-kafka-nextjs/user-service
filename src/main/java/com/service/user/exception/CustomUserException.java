package com.service.user.exception;

public class CustomUserException extends RuntimeException {
    public CustomUserException(String message, Throwable cause) {
        super(message, cause);
    }
}
